$(document).ready(function(){
    // popUp
    $('.emoji-option').click(function(){
        $(this).parentsUntil(".card-body").find('.floating-tag-option').toggleClass('show')
    })
    // random background emoji
    const srcPath = $('.emoji-background').attr('src')
    const emojiImage = String(Math.floor(Math.random() * 8) + 1) + '.png';
    $('.emoji-background').attr('src', srcPath+emojiImage)

    // accordio animation
    let i = 0; 
    $(".accordio").each(function(){
        $(this).css({
            "animationDelay" :  String(i) + "s",
            "animationDirection" : "reverse"
        })
        $(this).addClass("popUp")
        i += .4
    })

    // accordio click
    $('.accordio-title .title').click(function(){
        var id = $(this).html()
        $(".accordio-title .title").each(function(){
            $(this).parentsUntil(".accordio-list").find('.accordio-body').removeClass('show')
        })
        $(this).parentsUntil(".accordio-list").find('.accordio-body').toggleClass('show')
    })

    $('.button .button-up').click( function(){
        // ambil parentnya
        const thisAcordio = $(this).parent().parent().parent()
        var accordioBefore;

        // check apakah di atas ada, kalau ga ada ambil paling bawah
        if (thisAcordio.prev(".accordio").length) {
            accordioBefore = thisAcordio.prev(".accordio")
        } else {
            accordioBefore = $(".accordio:last-child")
        }

        // animasi hilang
        thisAcordio.removeClass("popUp")
        thisAcordio.width()
        thisAcordio.css({
            "animationDelay" :  "0s",
            "animationDirection" : "normal"
        })
        thisAcordio.addClass("popUp")
        
        accordioBefore.removeClass("popUp")
        accordioBefore.width()



        // untuk ngubah ke bawah
        const thisAcordioTitle = thisAcordio.find(".accordio-title .title").html()
        const thisAcordioBody = thisAcordio.find(".accordio-body").html()
        
        // masukin dalam variabel dulu, untuk ngubah ke atas
        const acordioBeforeTitle = accordioBefore.find(".accordio-title .title").html()
        const acordioBeforeBody = accordioBefore.find(".accordio-body").html()

        // mengubah contentnya
        accordioBefore.find(".accordio-title .title").html(thisAcordioTitle)
        accordioBefore.find(".accordio-body").html(thisAcordioBody)
        thisAcordio.find(".accordio-title .title").html(acordioBeforeTitle)
        thisAcordio.find(".accordio-body").html(acordioBeforeBody)

        // animation kembaliin
        thisAcordio.removeClass("popUp")
        thisAcordio.width()
            thisAcordio.css({
                "animationDelay" :  ".2s",
                "animationDirection" : "reverse"
            })
        thisAcordio.addClass("popUp")

        accordioBefore.removeClass("popUp")
        accordioBefore.width()
            accordioBefore.css({
                "animationDelay" :  ".7s",
                "animationDirection" : "reverse"
            })
        accordioBefore.addClass("popUp")
    })

    $('.button .button-down').click(function(){
        // ambil parentnya
        const thisAcordio = $(this).parent().parent().parent()

        // check apakah di atas ada, kalau ga ada ambil paling bawah
        if (thisAcordio.next(".accordio").length) {
            var accordioBefore = thisAcordio.next(".accordio")
        } else {
            var accordioBefore = thisAcordio.parent().children().first()
        }

        // animasi hilang
        thisAcordio.removeClass("popUp")
        thisAcordio.width()
        thisAcordio.css({
            "animationDelay" :  "0s",
            "animationDirection" : "normal"
        })
        thisAcordio.addClass("popUp")
        
        accordioBefore.removeClass("popUp")
        accordioBefore.width()

        // untuk ngubah ke bawah
        const thisAcordioTitle = thisAcordio.find(".accordio-title .title").html()
        const thisAcordioBody = thisAcordio.find(".accordio-body").html()
        
        // masukin dalam variabel dulu, untuk ngubah ke atas
        const acordioBeforeTitle = accordioBefore.find(".accordio-title .title").html()
        const acordioBeforeBody = accordioBefore.find(".accordio-body").html()

        

        // mengubah contentnya
        accordioBefore.find(".accordio-title .title").html(thisAcordioTitle)
        accordioBefore.find(".accordio-body").html(thisAcordioBody)
        thisAcordio.find(".accordio-title .title").html(acordioBeforeTitle)
        thisAcordio.find(".accordio-body").html(acordioBeforeBody)

        // animation kembaliin
        thisAcordio.removeClass("popUp")
        thisAcordio.width()
            thisAcordio.css({
                "animationDelay" :  ".2s",
                "animationDirection" : "reverse"
            })
        thisAcordio.addClass("popUp")

        accordioBefore.removeClass("popUp")
        accordioBefore.width()
            accordioBefore.css({
                "animationDelay" :  ".7s",
                "animationDirection" : "reverse"
            })
        accordioBefore.addClass("popUp")
    })
    

})

// deleteBtn
function deleteId(id) {
    $('#deleteForm #deleteId').attr('value', id)
}


// story 9
// generateAllLike
let allBookHaveLike = []
let leaderboard = []
function generateAllLike() {
    let byLike = []
    $.ajax({
        url: window.location.origin + '/api/like/',
        type: 'get',
        dataType: 'json',
        success: function(result){
            byLike = result
            allBookHaveLike = byLike.slice(0)
            allBookHaveLike.sort(function(a,b){
                return a.like - b.like
            })
            leaderboard = allBookHaveLike.slice(-6, -1)
            leaderboard.reverse()
        }
    })
}

// cek dulu host nya
if (window.location.href === window.location.origin + '/book/') {
    generateAllLike()
}

// searchBook
function searchBook() {
    $.ajax({
        url: 'https://www.googleapis.com/books/v1/volumes',
        type: 'get',
        dataType: 'json',
        data: {
            'q': $('#input-search').val(),
            'maxResults': 12,
        },
        success: function(result) {
            const book = result.items
            $("#book-list").html('')
            $.each(book, function(i, data){
                const info = data.volumeInfo
                let authors = ''
                let thumbnail = ''
                $.each(info.authors, function(i, data){
                    if (i === 0) {
                        authors += data
                    } else {
                        authors += ', ' + data
                    }
                })
                try {
                    thumbnail = info.imageLinks.thumbnail
                } catch (TypeError) {
                    thumbnail = '/static/img/image-not-found.png'
                }
                $("#book-list").append(`
                <div class="card" name="${data.id}">
                    <div style="background-image: url('${thumbnail}');" class="card-img" alt="${thumbnail}"></div>
                    <div class="card-body">
                        <h5 class="card-title">${info.title}</h5>
                        <span class="card-publisher">${info.publisher}</span>
                        <span class="card-athor">${authors}</span>
                        <span class="material-icons like">favorite_border</span>
                        <div class="flying-like"><span class="like-number">+1 </span><span class="material-icons like-full">favorite</span></div>
                    </div>
                </div>
                `)
            })
        }
    })
}

// input click
$('#input-search').on('keyup', function(e) {
    if (e.which === 13 ) {
        searchBook()
        $(this).val('')
    }
})

// search click
$('#button-search').on('click', function() {
    searchBook()
    $('#input-search').val('')
})

// like di click
$('#book-list').on('click','span.like', function(){
    const flyingLike = $(this).parent().find('.flying-like')
    const id = $(this).parent().parent().attr('name')
    const bookName = $(this).parent().find('.card-title').text()
    const thumbnailBig = $(this).parent().parent().find('.card-img').attr('alt')
    let flag = true
    flyingLike.removeClass("show")
    flyingLike.width()
    flyingLike.addClass("show")
    
    $.each(allBookHaveLike, function(i, data){
        if(data.bookID.toLowerCase() == id.toLowerCase()){
            dataPlus = {
                'bookID' : data.bookID,
                'bookName' : data.bookName,
                'thumbnailBig' : data.thumbnailBig,
                'like' : Number(data.like) + 1,
                'csrfmiddlewaretoken': $('[name=csrfmiddlewaretoken]').val()
            }
            plusLikeData(data.url, dataPlus)
            flag = false
            return flag
        }
    })
    if (flag) {
        dataAdd = {
            'bookID': id,
            'bookName' : bookName,
            'thumbnailBig' : thumbnailBig,
            'like': 1,
            'csrfmiddlewaretoken': $('[name=csrfmiddlewaretoken]').val()
        }
        addLikeData(dataAdd)
    }
    generateAllLike()
    
})

// nambah like kalau data udah ada
function plusLikeData(url, bookID, like){
    $.ajax({
        url: url,
        type: 'post',
        dataType: 'json',
        data: dataPlus,
    })
}

// nambah data like
function addLikeData(dataAdd){
    $.ajax({
        url: window.location.origin + '/api/like/',
        type: 'post',
        dataType: 'json',
        data: dataAdd,
    })
}

leaderboard
$('#leaderboard-btn').on('click', function(){
    $('.modal-body').html('')
    $.each(leaderboard, function(i, data){
        $('.modal-body').append(`
            <div class="leaderboard-card">
            <div style="background-image: url('${data.thumbnailBig}');" class="leaderboard-img" alt="${data.thumbnailBig}"></div>
                <div class="leaderboard-title">
                ${data.bookName}
                </div>
                <div class="leaderboard-like"><span>${data.like}</span><span class="material-icons">favorite</span></div>
            </div>
        `)
    })
})


