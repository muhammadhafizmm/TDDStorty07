from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Comment
from django.contrib import messages
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required
def index(request):
    if request.method == "POST":
        if 'deleteId' in request.POST:
            try:
                deleteComment = Comment.objects.get(id=request.POST['deleteId'])
                deleteComment.delete()
                messages.success(request, "Komentar berhasi di hapus!")
            except:
                messages.error(request, "Wahh.. Terjadi error ni pada website, ayo refresh dulu websitenya!")
        elif 'color' in request.POST:
            comment = Comment.objects.get(id=request.POST['id'])
            comment.color = request.POST['color']
            comment.save()
            messages.success(request, "YAAYYY, tag berhasil dirubah")
    comment = Comment.objects.all().order_by("-id")
    return render(request, "commentSection/index.html", {"commentList" : comment})

@login_required
def create(request):
    if request.method == "POST":
        if request.POST["confirm"] == "false":
            messages.error(request, "Yaaahh, komentar mu batal di post deh!")
            return redirect("/commentsection/")
        data = request.POST
        new_comment = Comment.objects.create(nama=data['nama'], komentar=data['komentar'], color=data['color'])
        new_comment.save()
        messages.success(request, "Yey, komentar kamu berhasil di post!")
        return redirect("/commentsection/")

    form = request.GET
    return render(request, "commentSection/create.html", {'form' : form})