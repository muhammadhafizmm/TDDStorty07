from django.apps import AppConfig


class CommentsectionConfig(AppConfig):
    name = 'commentSection'
