from django.shortcuts import render
from rest_framework import viewsets, permissions, status
from .models import Like
from .serializers import LikeSerializer
from rest_framework.decorators import api_view
from rest_framework.response import Response


# Create your views here.

# class LikeView(viewsets.ModelViewSet):
#     serializer_class = LikeSerializer
#     queryset = Like.objects.all()

@api_view(['GET', 'POST'])
def likeOverview(request):
    serializer_context = {
        'request': request,
    }
    if request.method == 'GET':
        likes = Like.objects.all()
        serializer = LikeSerializer(likes, many=True, context=serializer_context)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = LikeSerializer(data=request.data, context=serializer_context)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET', 'POST', 'DELETE'])
def likeOverviewDetail(request, pk):
    serializer_context = {
        'request': request,
    }
    try:
        like = Like.objects.get(pk=pk)
    except like.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = LikeSerializer(like, context=serializer_context)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = LikeSerializer(like, data=request.data, context=serializer_context)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        like.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


