# STORY LANJUTAN REPO
Hi, ini adalah repo story lanjutan untuk matkul PPW. Nanti semua project dari story 7 sampai seterusnya mungkin akan di kerjakan di sini. Mantap!

## Story 7 "Komentar"
Di story 7 ini, kita diminta membuat sebuah project comment section, tapi sistem pembuatannya diminta menggunakan konsep TDD. TDD yang di lakukan tidak hanya unittesting seperti story sebelumnya, tapi juga harus sudah melakkuan functional test didalamnya menggunakan Selenium.
<br>

## Story 8 "Accordio"
Di Story 8 ini kita belajar tentang jquery dan javascript. Tutorialnya banyak di youtube dan w3school. Jangan di biasakan untuk menulis jquery karena kalau lebih ngerti jquery dari pada javascript, bisa susah untuk mamu membuat fitur yang custom gitu.
Jquery itu cuma framework yang membantu menulis javascript dengan cepat, seperti halnya bootstrapt untuk css. Tapi untuk beberapa fitur yang mudah, tidak terlalu ribet jquery sangat berguna.

## Story 9 "Book APIs and Like"
Di Story 9 ini kita diminta untuk mengerti tentang Ajax dan WebService, dimana kita dikasih sebuah public APIs dari google book, yang memberikan kembalian sebuah data buku berdasarakan keyword yang di pencarian. Disini kita harus membuat sebuah search box yang di dalamnya user akan mengketikan keyword yang mau di cari, nanti dari situ kita bisa ngambil data bukunya di API yang sudah di sediakan. Dari situ juga kita disuruh membuat sistem like dan ranking dari buku yang ada.

## Story 10 "Login dan Register"
Di Story 10 ga ada yang spesial cuma login dan register.

### Status Repo
Pipeline Status<br>
[![pipeline status](https://gitlab.com/muhammadhafizmm/TDDStorty07/badges/master/pipeline.svg)](https://gitlab.com/muhammadhafizmm/TDDStorty07/-/commits/master)<br>
Coverage Status<br>
[![coverage report](https://gitlab.com/muhammadhafizmm/TDDStorty07/badges/master/coverage.svg)](https://gitlab.com/muhammadhafizmm/TDDStorty07/-/commits/master)