from django.test import TestCase

# Create your tests here.
from django.test import TestCase, Client
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from . import apps
from .models import Comment
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import *
import time

# Create your tests here.
class LandingPageUnitTest(TestCase):
    # Test App
    def test_apps(self):
        self.assertEqual(apps.LandingpageConfig.name, 'landingpage')

    # Url is Exits, Comment Section
    def landing_page_section_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def account_login_section_url_is_exist(self):
        response = Client().get('/account/')
        self.assertEqual(response.status_code, 200)

    # Url is Use Correct Template, Comment Section
    def landing_page_section_url_is_using_corect_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'landingpage/index.html')
    
    # Url is Use Correct Template, Comment Section
    def account_login_section_url_is_using_corect_template(self):
        response = Client().get('/account/')
        self.assertTemplateUsed(response, 'landingpage/login.html')
    


class TDDStrory7FunctionalTest(StaticLiveServerTestCase):
    def setUp(self):
        chrome_options = Options()

        # kalau mau di push ke gitlab
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        # di lokal pake exe, soalnya windows. kalau di gitlab exenya di hapus karena gitlab pake linux
        self.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(TDDStrory7FunctionalTest, self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(TDDStrory7FunctionalTest, self).tearDown()

    def test_todo(self):
        browser = self.browser
        browser.get(self.live_server_url +'/account')
        time.sleep(5)
        self.assertIn("Login", browser.title)
