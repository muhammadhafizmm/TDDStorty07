from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib import messages
from django.contrib.auth.signals import user_logged_in, user_logged_out
from django.contrib.auth.forms import UserCreationForm

def index(request):
    return render(request, "landingPage/index.html")

def register(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, f'Selamat, Akun {username} telah dibuat!')
            print(username)
            return redirect('landingpage:login')
        else:
            messages.error(request, 'Wahh sepertinya ada yang salah di pengisian data mu!')
            return render(request, 'landingPage/register.html', {"form" : form})
    else:
        form = UserCreationForm()
        return render(request, "landingPage/register.html", {"form" : form})

def loginSuccessMessage(sender, user, request, **kwargs):
    messages.success(request, "Halo " + user.username + "! Kamu berhasil Login.")

def logoutSuccessMessage(sender, user, request, **kwargs):
    messages.success(request, "Terimakasih " + user.username + " telah berkunjung. Dadah!")

user_logged_in.connect(loginSuccessMessage)
user_logged_out.connect(logoutSuccessMessage)